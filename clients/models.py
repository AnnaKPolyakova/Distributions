import pytz
from django.db import models

from clients.validators import phone_number_validator


class Tag(models.Model):
    title = models.CharField(
        unique=True,
        max_length=255,
        verbose_name="Наименование",
        help_text="Укажите наименование тэга",
    )

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self):
        return self.title


class Code(models.Model):
    code = models.IntegerField(
        unique=True,
        verbose_name="Код мобильного оператора",
        help_text="Укажите код мобильного оператора",
    )

    class Meta:
        verbose_name = "Код мобильного оператора"
        verbose_name_plural = "Коды мобильных операторов"

    def __str__(self):
        return f"{self.code}"


class Client(models.Model):
    TIMEZONE_CHOICES = zip(pytz.all_timezones, pytz.all_timezones)

    code = models.ForeignKey(
        Code,
        on_delete=models.CASCADE,
        related_name="clients",
        verbose_name="Код мобильного оператора",
        help_text="Укажите код мобильного оператора",
    )
    phone_number = models.CharField(
        unique=True,
        max_length=11,
        validators=[phone_number_validator],
        verbose_name="Рабочий номер телефона",
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        related_name="tag",
        verbose_name="Тэг",
        help_text="Укажите тэг для фильтрации клиентов для осуществления "
                  "рассылки",
    )
    time_zone = models.CharField(choices=TIMEZONE_CHOICES, max_length=40)

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return f"{self.code}"
