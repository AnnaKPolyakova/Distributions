from django.core.exceptions import ValidationError

PHONE_NUMBER_ERROR_MESSAGE = (
    "Номер телефона допустимо указывать только в "
    "следующем формате: 7XXXXXXXXXX (X - цифры от 0 до 0)"
)


def phone_number_validator(value):
    if value[0] != "7" or len(value) != 11 or not value[1:].isdigit():
        raise ValidationError(PHONE_NUMBER_ERROR_MESSAGE)
