import factory
import pytz
from django.contrib.auth import get_user_model

from clients.models import Client, Code, Tag

User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ["email", "username"]

    password = factory.PostGenerationMethodCall("set_password", "admin")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.LazyAttribute(
        lambda a: "{}.{}".format(
            a.first_name, a.last_name).lower()
    )
    email = factory.LazyAttribute(
        lambda a: "{}.{}@test.com".format(
            a.first_name, a.last_name).lower()
    )
    is_staff = True
    is_superuser = True


class TagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tag
        django_get_or_create = ["title"]

    title = factory.Faker("sentence", locale="ru_RU", nb_words=2)


class CodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Code
        django_get_or_create = ["code"]

    code = factory.Faker("random_int", min=100, max=99999, step=1)


class ClientFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Client
        django_get_or_create = ["phone_number"]

    code = factory.Iterator(Code.objects.all())
    phone_number = factory.Faker(
        "random_int", min=70000000000, max=79999999999, step=1
    )
    tag = factory.Iterator(Tag.objects.all())
    time_zone = factory.Iterator(
        pytz.all_timezones,
        getter=lambda choice: choice[0],
    )
