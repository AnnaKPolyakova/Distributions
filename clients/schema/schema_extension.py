from clients.serializers import ClientSerializer
from clients.validators import PHONE_NUMBER_ERROR_MESSAGE

ERROR_MESSAGES_FOR_ADD_CLIENT_400 = {
    "example": {
        "tag": ["Обязательное поле.", "Это поле не может быть пустым."],
        "code": ["Обязательное поле.", "Это поле не может быть пустым."],
        "phone_number": [
            "Обязательное поле.",
            PHONE_NUMBER_ERROR_MESSAGE,
            "Клиент с таким Рабочий номер телефона уже существует.",
            "Убедитесь, что это значение содержит не более 11 символов.",
        ],
        "time_zone": ["Обязательное поле.", "Это поле не может быть пустым."],
    },
}


ERROR_MESSAGES_FOR_PARTIAL_UPDATE_CLIENT_400 = {
    "example": {
        "phone_number": [
            PHONE_NUMBER_ERROR_MESSAGE,
            "Клиент с таким Рабочий номер телефона уже существует.",
            "Убедитесь, что это значение содержит не более 11 символов.",
        ],
    },
}


CLIENT_RESPONSE_FOR_CREATE = {
    201: ClientSerializer,
    400: ERROR_MESSAGES_FOR_ADD_CLIENT_400,
}

CLIENT_RESPONSE_FOR_UPDATE = {
    200: ClientSerializer,
    400: ERROR_MESSAGES_FOR_ADD_CLIENT_400,
}

CLIENT_RESPONSE_FOR_PARTIAL_UPDATE = {
    200: ClientSerializer,
    400: ERROR_MESSAGES_FOR_PARTIAL_UPDATE_CLIENT_400,
}
