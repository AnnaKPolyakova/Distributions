from django.contrib import admin
from django.contrib.admin import register

from clients.models import Client, Code, Tag


@register(Tag)
class TagAdmin(admin.ModelAdmin):

    list_display = ("title",)
    list_filter = ("title",)


@register(Code)
class CodeAdmin(admin.ModelAdmin):

    list_display = ("code",)
    list_filter = ("code",)


@register(Client)
class ClientAdmin(admin.ModelAdmin):

    list_display = ("code", "phone_number", "tag", "time_zone")
    list_filter = ("code", "phone_number", "tag", "time_zone")
