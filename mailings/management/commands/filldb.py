from typing import Any, Optional

from django.core.management.base import BaseCommand, CommandError

from clients.tests.factories import (ClientFactory, CodeFactory, TagFactory,
                                     UserFactory)
from mailings.tests.factories import MailingFactory, MessageFactory


def notification(command, objects, text):
    command.stdout.write(
        command.style.SUCCESS(f"{len(objects)} {text} успешно создано.")
    )


class Command(BaseCommand):
    help = (
        "Заполняет БД тестовыми данными. Сейчас доступны:"
        " - Теги,"
        " - Коды,"
        " - Клиенты"
    )

    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        try:
            users = UserFactory.create_batch(5)
            notification(self, users, "users")

            user = UserFactory(
                username="admin",
                email="admin@admin.ru"
            )
            notification(
                self,
                [user],
                ("администратор с username admin и паролем admin")
            )

            tags = TagFactory.create_batch(5)
            notification(self, tags, "tags")

            codes = CodeFactory.create_batch(5)
            notification(self, codes, "codes")

            clients = ClientFactory.create_batch(5)
            notification(self, clients, "clients")

            mailings = MailingFactory.create_batch(5)
            notification(self, mailings, "mailings")

            messages = MessageFactory.create_batch(5)
            notification(self, messages, "messages")

        except CommandError:
            self.stdout.write(self.style.ERROR("Ошибка наполнения БД"))
