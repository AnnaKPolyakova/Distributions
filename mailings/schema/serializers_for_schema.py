from rest_framework import serializers

from mailings.models import Mailing
from mailings.serializers import MessageGetSerializer


class MailingGetListForSchemaSerializer(serializers.ModelSerializer):

    total_message = serializers.IntegerField()
    send = serializers.IntegerField()
    not_send = serializers.IntegerField()
    tag = serializers.CharField(source="tag.title")
    code = serializers.IntegerField()

    class Meta:
        model = Mailing
        fields = "__all__"


class MailingGetDetailForSchemaSerializer(serializers.ModelSerializer):

    total_message = serializers.IntegerField()
    send = serializers.IntegerField()
    not_send = serializers.IntegerField()
    tag = serializers.CharField(source="tag.title")
    code = serializers.CharField(source="code.code")
    messages = serializers.ListField(child=MessageGetSerializer())

    class Meta:
        model = Mailing
        fields = "__all__"
