from mailings.schema.serializers_for_schema import (
    MailingGetDetailForSchemaSerializer, MailingGetListForSchemaSerializer)
from mailings.serializers import MailingSerializer

DATA_TIME_ERROR_MESSAGES = (
    [
        "Обязательное поле.",
        "Неправильный формат datetime. Используйте один из этих форматов:  "
        "YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z].",
    ],
)

ERROR_MESSAGES_FOR_ADD_MAILING_400 = {
    "example": {
        "non_field_errors": [
            "Время окончания рассылки должно быть больше, чем время начала"
        ],
        "start": DATA_TIME_ERROR_MESSAGES,
        "end": DATA_TIME_ERROR_MESSAGES,
        "text": ["Обязательное поле.", "Это поле не может быть пустым."],
        "tag": ["Обязательное поле.", "Это поле не может быть пустым."],
        "code": ["Обязательное поле.", "Это поле не может быть пустым."],
    },
}


ERROR_MESSAGES_FOR_PARTIAL_UPDATE_MAILING_400 = {
    "example": {
        "non_field_errors": [
            "Время окончания рассылки должно быть больше, чем время начала"
        ],
        "start": DATA_TIME_ERROR_MESSAGES,
        "end": DATA_TIME_ERROR_MESSAGES,
    },
}


MAILING_RESPONSE_FOR_CREATE = {
    201: MailingSerializer,
    400: ERROR_MESSAGES_FOR_ADD_MAILING_400,
}

MAILING_RESPONSE_FOR_UPDATE = {
    200: MailingSerializer,
    400: ERROR_MESSAGES_FOR_ADD_MAILING_400,
}

MAILING_RESPONSE_FOR_PARTIAL_UPDATE = {
    200: MailingSerializer,
    400: ERROR_MESSAGES_FOR_PARTIAL_UPDATE_MAILING_400,
}

MAILING_RESPONSE_FOR_LIST = {
    200: MailingGetListForSchemaSerializer,
}

MAILING_RESPONSE_FOR_RETRIEVE = {
    200: MailingGetDetailForSchemaSerializer,
}
