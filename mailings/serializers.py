from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from mailings.models import ERROR_MESSAGE_FOR_MAILING, Mailing, Message


class MessageGetSerializer(serializers.ModelSerializer):
    client = serializers.CharField(source="client.phone_number")

    class Meta:
        model = Message
        exclude = ("mailing",)


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = "__all__"

    def validate(self, data):
        if self.context["request"].method not in ("POST", "PUT"):
            return data
        if data["start"] >= data["end"]:
            raise ValidationError(ERROR_MESSAGE_FOR_MAILING)
        return data


class MailingGetListSerializer(serializers.ModelSerializer):

    total_message = serializers.SerializerMethodField()
    send = serializers.SerializerMethodField()
    not_send = serializers.SerializerMethodField()
    tag = serializers.CharField(source="tag.title")
    code = serializers.CharField(source="code.code")

    class Meta:
        model = Mailing
        fields = "__all__"

    def get_total_message(self, obj):
        return obj.messages.all().count()

    def get_send(self, obj):
        return obj.messages.filter(status=Message.Status.SENT).count()

    def get_not_send(self, obj):
        return obj.messages.filter(status=Message.Status.NOT_SENT).count()


class MailingGetDetailSerializer(serializers.ModelSerializer):

    total_message = serializers.SerializerMethodField()
    send = serializers.SerializerMethodField()
    not_send = serializers.SerializerMethodField()
    tag = serializers.CharField(source="tag.title")
    code = serializers.CharField(source="code.code")
    messages = serializers.SerializerMethodField()

    class Meta:
        model = Mailing
        fields = "__all__"

    def get_total_message(self, obj):
        return obj.messages.all().count()

    def get_send(self, obj):
        return obj.messages.filter(status=Message.Status.SENT).count()

    def get_not_send(self, obj):
        return obj.messages.filter(status=Message.Status.NOT_SENT).count()

    def get_messages(self, obj):
        messages = Message.objects.filter(mailing=obj)
        serializer = MessageGetSerializer(messages, many=True)
        return serializer.data
