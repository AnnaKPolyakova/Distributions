import pytest
from django.urls import reverse

MAILING_URL = reverse("mailings-list")
MAILING_DETAIL_URL = "mailings-detail"
TEXT = "test"
TEXT_2 = "test_2"
START = "2022-03-24 19:30"
END = "2024-10-13 10:10"

pytestmark = pytest.mark.django_db


class TestMailingAPI:

    def test_create_mailing_url(self, authorized_client, code, tag):
        url = MAILING_URL
        data = {
            "start": START,
            "end": END,
            "text": TEXT,
            "tag": tag.id,
            "code": code.id,
        }
        response = authorized_client.post(url, data=data)
        assert response.status_code == 201, (
            f"Проверьте, что при POST запросе"
            f"{url} возвращается статус 201"
        )

    def test_update_mailing_url(self, authorized_client, code, tag, mailing):
        url = reverse(MAILING_DETAIL_URL, args=[mailing.id])
        data = {
            "start": START,
            "end": END,
            "text": TEXT,
            "tag": tag.id,
            "code": code.id,
        }
        response = authorized_client.put(url, data=data)
        assert response.status_code == 200, (
            f"Проверьте, что при PUT запросе"
            f"{url} возвращается статус 200"
        )
        response = authorized_client.patch(url, data=data)
        assert response.status_code == 200, (
            f"Проверьте, что при PATCH запросе"
            f"{url} возвращается статус 200"
        )

    def test_delete_mailing_url(self, authorized_client, mailing):
        url = reverse(MAILING_DETAIL_URL, args=[mailing.id])
        response = authorized_client.delete(url)
        assert response.status_code == 204, (
            f"Проверьте, что при DELETE запросе"
            f"{url} возвращается статус 204"
        )

    def test_get_mailing_url(self, authorized_client, mailing):
        url = reverse(MAILING_DETAIL_URL, args=[mailing.id])
        response = authorized_client.get(url)
        assert response.status_code == 200, (
            f"Проверьте, что при GET запросе"
            f"{url} возвращается статус 200"
        )

    def test_get_list_mailing_url(self, authorized_client, mailing):
        url = MAILING_URL
        response = authorized_client.get(url)
        assert response.status_code == 200, (
            f"Проверьте, что при GET запросе"
            f"{url} возвращается статус 200"
        )
