import random

import factory
import pytz
from django.db.models import Q

from clients.models import Client, Code, Tag
from clients.tests.factories import ClientFactory
from mailings.models import Mailing, Message


class MailingFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Mailing

    start = factory.Faker(
        "date_time_this_year", before_now=True, tzinfo=pytz.UTC
    )
    end = factory.Faker(
        "date_time_this_year", after_now=True, tzinfo=pytz.UTC
    )
    text = factory.Faker("sentence", locale="ru_RU", nb_words=5)
    code = factory.Iterator(Code.objects.all())
    tag = factory.Iterator(Tag.objects.all())


class MessageFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Message

    time = factory.Faker("date_this_year", before_today=True)
    status = factory.Iterator(
        Message.Status.choices,
        getter=lambda choice: choice[0],
    )
    mailing = factory.Iterator(Mailing.objects.all())

    @factory.lazy_attribute
    def client(self):
        client_ids = Message.objects.filter(
            mailing=self.mailing).values_list("client_id")
        if Client.objects.filter(~Q(id__in=client_ids)).exists():
            return random.choice(Client.objects.filter(~Q(id__in=client_ids)))
        return ClientFactory()
