import logging
import os

from config.settings import settings

app_name = __package__.rsplit('.', 1)[-1]
log_file_path = app_name + "/" + app_name + ".log"
log_file = logging.FileHandler(
    os.path.join(settings.BASE_DIR, log_file_path)
)
log_file.setLevel(logging.DEBUG)
format_str = '[%(asctime)s] [%(name)s] [%(levelname)s] > %(message)s'
format_date = '%Y-%m-%d %H:%M:%S'
log_format = logging.Formatter(fmt=format_str, datefmt=format_date)
log_file.setFormatter(log_format)

mailing_views_logger = logging.getLogger('mailing_views_logger')
mailing_views_logger.setLevel(logging.DEBUG)
mailing_views_logger.addHandler(log_file)

mailing_tasks_logger = logging.getLogger('mailing_tasks_logger')
mailing_tasks_logger.setLevel(logging.DEBUG)
mailing_tasks_logger.addHandler(log_file)
