from django.db import transaction
from django.utils import timezone
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from mailings.loggers import mailing_views_logger
from mailings.models import Mailing
from mailings.schema.schema_extension import (
    MAILING_RESPONSE_FOR_CREATE, MAILING_RESPONSE_FOR_LIST,
    MAILING_RESPONSE_FOR_PARTIAL_UPDATE, MAILING_RESPONSE_FOR_RETRIEVE,
    MAILING_RESPONSE_FOR_UPDATE)
from mailings.serializers import (MailingGetDetailSerializer,
                                  MailingGetListSerializer, MailingSerializer)
from mailings.tasks import mailing_task

MAIL_OBJECT_CREATE = "Рассылка c номером id {id} создана"


@extend_schema_view(
    create=extend_schema(responses=MAILING_RESPONSE_FOR_CREATE),
    update=extend_schema(responses=MAILING_RESPONSE_FOR_UPDATE),
    partial_update=extend_schema(
        responses=MAILING_RESPONSE_FOR_PARTIAL_UPDATE
    ),
    list=extend_schema(
        responses=MAILING_RESPONSE_FOR_LIST,
    ),
    retrieve=extend_schema(responses=MAILING_RESPONSE_FOR_RETRIEVE),
)
class MailingViewSet(
    viewsets.ModelViewSet,
):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == "list":
            return MailingGetListSerializer
        elif self.action == "retrieve":
            return MailingGetDetailSerializer
        return self.serializer_class

    @transaction.atomic
    def perform_create(self, serializer):
        mailing = serializer.save()
        mailing_views_logger.info(
            MAIL_OBJECT_CREATE.format(id=mailing.id)
        )
        if mailing.start <= timezone.now() < mailing.end:
            transaction.on_commit(
                lambda: mailing_task.delay(
                    mailing.id,
                )
            )
        elif mailing.start > timezone.now() < mailing.end:
            transaction.on_commit(
                lambda: mailing_task.apply_async(
                    (mailing.id,),
                    eta=mailing.start
                )
            )
